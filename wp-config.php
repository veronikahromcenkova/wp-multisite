<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wordpress_db' );

/** MySQL database username */
define( 'DB_USER', 'admin' );

/** MySQL database password */
define( 'DB_PASSWORD', 'password' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'K2]uT02Zneje)LSt,XVWo)K&5|Vq{;-z%R%xvvPpB>[i>L{:RO}3^|K-l4+?-C$*' );
define( 'SECURE_AUTH_KEY',  'E[po0t(ERs+jR,<Q4RYHn1=_2A-(Pvm2.o0{Wz>McZu}|iHV32`.*T*e;l8L12!(' );
define( 'LOGGED_IN_KEY',    'JY,2f6;d>YBtg]P)?1mC,h:hy9o?.$EGp{`WxV[N[M|8w3y$A|CcB(&D)c@2F_ah' );
define( 'NONCE_KEY',        'RWwJDvYu,<-?l>@<+>deAfL@]Pf2y-BVb<=Aa;L0f^BLXBu3X:4CK|It(yhsq[6K' );
define( 'AUTH_SALT',        '!_y&:&d sHC|9sss<!.]^/(f5{ZEj6^bn-Pfr6T~I-O:4!jtA559M1_MXB.Z7UDD' );
define( 'SECURE_AUTH_SALT', 'A<`ZPr_pB{beI>@m[[Xqj.kme,lr$Q$x1M9S3In?P1Bdf+`VaT $UWm%7RaGX`Rc' );
define( 'LOGGED_IN_SALT',   'cn!U2S#``o4NuDiiP%pXXN(lke?9Sv4`89eyk9h?VK=BRpkojwZQi-t `1v[U.X|' );
define( 'NONCE_SALT',       ')_{W_n$u7=G{jHr:0>=G5ot]iKWdZB{)HXRAN:>pRsKSD$l].S8L72*JKt>96h_[' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* Multisite */
define( 'WP_ALLOW_MULTISITE', true );
define('MULTISITE', true);
define('SUBDOMAIN_INSTALL', false);
define('DOMAIN_CURRENT_SITE', 'localhost');
define('PATH_CURRENT_SITE', '/wordpress/');
define('SITE_ID_CURRENT_SITE', 1);
define('BLOG_ID_CURRENT_SITE', 1);

define('FS_METHOD', 'direct');

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
